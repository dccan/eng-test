-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th2 25, 2019 lúc 06:48 PM
-- Phiên bản máy phục vụ: 10.1.38-MariaDB
-- Phiên bản PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_baitest`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `image`) VALUES
(1, 'Sự kiện', NULL),
(2, 'Kinh doanh', 'images/kotlin-meetup-2019.1-cover-6.png'),
(3, 'Giáo dục', 'images/Mindset_of_innovation.png'),
(4, 'Tin tức', ''),
(5, 'Gia đình', 'images/Eventpop-02.png'),
(6, 'Từ thiện', 'images/EventPOP_851x400.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `footer` text COLLATE utf8_unicode_ci NOT NULL,
  `img_app` text COLLATE utf8_unicode_ci NOT NULL,
  `img_call` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `config`
--

INSERT INTO `config` (`id`, `footer`, `img_app`, `img_call`) VALUES
(1, '<div class=\"pdLeft col-sm-12 col-md-8\">\r\n                            <div class=\"spacer-sm visible-xs\"></div>\r\n                            <div class=\"text-heading mb-4 text-primary\">\r\n                              <strong>Công ty du lich kỳ nghĩ toàn cầu</strong>\r\n                            </div>\r\n                            <div class=\"footer-contents\">\r\n                              <ul class=\"links list-unstyled\">\r\n                                <li class=\"hcm\">Hồ Chí Minh</li>\r\n                                <li >\r\n                                    <i class=\"glyphicon glyphicon-map-marker\" ></i>  123 Đườn 45, Phường 14, Q.Tân Bình, Hồ Chí Minh.\r\n                                </li>\r\n                                <li>\r\n                                    <i class=\"glyphicon glyphicon-earphone\" ></i> 123-456-798\r\n                                </li>\r\n                                <li>\r\n                                    <i class=\"glyphicon glyphicon-phone-alt\" ></i> 123-456-798\r\n                                </li>\r\n\r\n                                <li class=\"hn\">Hà Nội</li>\r\n                                <li >\r\n                                    <i class=\"glyphicon glyphicon-map-marker\" ></i>  123 Đườn 45, Phường 14, Q.Tân Bình, Hà Nội.\r\n                                </li>\r\n                                <li>\r\n                                    <i class=\"glyphicon glyphicon-earphone\" ></i> 123-456-798\r\n                                </li>\r\n                                <li>\r\n                                    <i class=\"glyphicon glyphicon-phone-alt\" ></i> 123-456-798\r\n                                </li>\r\n\r\n                                <li class=\"mail\">\r\n                                    <i class=\"glyphicon glyphicon-globe\" ></i> www.abc.com.vn\r\n                                </li>\r\n                                <li>\r\n                                    <i class=\"glyphicon glyphicon-envelope\" ></i> \r\n                                    abc@tc.com\r\n                                </li>\r\n                              </ul>\r\n                            </div>\r\n                          </div>\r\n\r\n                          <div class=\"col-sm-12 col-md-4\">\r\n                            <div class=\"spacer-sm visible-xs\"></div>\r\n                            <div class=\"text-heading mb-4 text-primary\">\r\n                              <strong>Về chúng tôi</strong>\r\n                            </div>\r\n                            <div class=\"footer-contents\">\r\n                              <ul class=\"links list-unstyled\">\r\n                                <li>\r\n                                  <a target=\"_blank\" href=\"\">Blog</a>\r\n                                </li>\r\n                                <li>\r\n                                  <a href=\"\">Press</a>\r\n                                </li>\r\n                                <li>\r\n                                    <a href=\"\">Careers</a>\r\n                                </li>\r\n                                <li>\r\n                                  <a href=\"\">Payment</a>\r\n                                </li>\r\n                                <li>\r\n                                  <a href=\"\">Payment</a>\r\n                                </li>\r\n                                <li>\r\n                                  <a href=\"\">Payment</a>\r\n                                </li>\r\n\r\n                                <li class=\"social\">\r\n                                    <img class=\"img-responsive\" src=\"images/icon-face.png\" alt=\"icon\">\r\n                                    <img class=\"img-responsive\" src=\"images/icon-line.png\" alt=\"icon\">\r\n                                    <img class=\"img-responsive\" src=\"images/icon-tw.png\" alt=\"icon\">\r\n                                    <img class=\"img-responsive\" src=\"images/icon-in.png\" alt=\"icon\">\r\n                                </li>\r\n\r\n                                <li class=\"app-mobile\">\r\n                                    <img class=\"img-responsive\" src=\"images/icon-pl.jpg\" alt=\"icon\">\r\n                                    <span>\r\n                                        <img class=\"img-responsive\" src=\"images/app.jpg\" alt=\"icon\">\r\n                                        <img class=\"img-responsive ch-play\" src=\"images/ch-play.jpg\" alt=\"icon\">\r\n                                    <span>\r\n                                </li>\r\n                              </ul>\r\n                            </div>\r\n                          </div>', 'images/img-down-app.jpg', 'images/call-me.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `local` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_event` datetime DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci,
  `slide` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `name`, `slug`, `local`, `time_event`, `category_id`, `image`, `slide`, `created_at`) VALUES
(1, 'Elephant Boat Race & River Festival 2019', NULL, 'Next to Anantara Riverside Bangkok Resort, Charoen ', '2019-02-28 19:00:00', 1, 'images/Event-Pop_Event-Cover_Full-Lineup.jpg', 1, '2019-02-28 19:00:00'),
(2, 'ONLINE REGISTER NOW!! G-SHOCK x TRANSFORMERS Limited Edition', NULL, 'G-SHOCK CASIO Flagship Store, 3fl @CentralWorld, Rama I Road, Pathum Wan, Bangkok, Thailand\r\n', '2019-02-28 13:00:00', 1, 'images/PJH_BKK__event_pop_1702ex.jpg', 1, '2019-02-27 00:00:00'),
(3, 'Chang Carnival Presents The Green World “Fantasy Ocean”', NULL, 'Ocean Marina Yacht Club, Na Chom Thian, Sattahip, Chon ', '2019-02-26 07:00:00', 1, 'images//TGW_PTY_EVENTPOP_1702x800.jpg', 1, NULL),
(4, 'Kolour In The Park 2019', NULL, 'THAI WAKE PARK - Lumlukka (TWP Lumlukka), ', '2019-02-23 03:00:00', 1, 'images/EVP_Media_size_6.png', 0, NULL),
(5, 'Rita Ora Live in Bangkok 2019', NULL, 'THAI WAKE PARK - Lumlukka (TWP Lumlukka), ', '2019-02-23 03:00:00', 1, 'images/PJH_BKK__event_pop_1702ex.jpg', 0, NULL),
(6, 'Rita Ora Live in Bangkok 2019', NULL, 'THAI WAKE PARK - Lumlukka (TWP Lumlukka), ', '2019-02-23 03:00:00', 1, 'images/cover_page2_(1)_(1).jpg', 0, NULL),
(7, 'KILORUN HANOI 2019', NULL, 'Lý Thái Tổ, เขตฮว่านเกี๋ยม Hanoi, ', '2019-02-27 00:00:00', 2, 'images/EVP_Kilorun_Hanoi_Cover_Banner_851_x_400_px.jpg', 0, NULL),
(8, 'KILORUN HANOI 2019 2', NULL, 'Lý Thái Tổ, เขตฮว่านเกี๋ยม Hanoi, ', '2019-02-27 00:00:00', 2, 'images/EVP_Kilorun_Hanoi_Cover_Banner_851_x_400_px.jpg', 0, NULL),
(9, 'KILORUN HANOI 2019 3', NULL, 'Lý Thái Tổ, เขตฮว่านเกี๋ยม Hanoi, ', '2019-02-27 00:00:00', 2, 'images/EVP_Kilorun_Hanoi_Cover_Banner_851_x_400_px.jpg', 0, NULL),
(10, 'KILORUN HANOI 2019 4', NULL, 'Lý Thái Tổ, เขตฮว่านเกี๋ยม Hanoi, ', '2019-02-27 00:00:00', 2, 'images/EVP_Kilorun_Hanoi_Cover_Banner_851_x_400_px.jpg', 0, NULL),
(11, 'Facebook Developer Circles', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 3, 'images/eventpop_FBf_b.jpg', 0, '2019-02-24 00:00:00'),
(12, 'Facebook Developer Circles 2', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 3, 'images/eventpop_FBf_b.jpg', 0, '2019-02-24 00:00:00'),
(13, 'Facebook Developer Circles 3', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 3, 'images/eventpop_FBf_b.jpg', 0, '2019-02-24 00:00:00'),
(14, 'Pepsi Presents S2O Songkran Music Festival 2019', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(15, 'DADJU à Empire | LUN 25 FEV', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(16, 'Pepsi Presents S2O Songkran Music Festival 2019', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(17, 'DADJU à Empire | LUN 25 FEV', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(18, 'Pepsi Presents S2O Songkran Music Festival 2019', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(19, 'DADJU à Empire | LUN 25 FEV', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(20, 'Pepsi Presents S2O Songkran Music Festival 2019', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(21, 'DADJU à Empire | LUN 25 FEV', NULL, NULL, NULL, 4, 'images/EVP_Media_size_6.png', 0, '2019-02-25 19:20:12'),
(22, 'Kolour In The Park 2019', NULL, 'THAI WAKE PARK - Lumlukka (TWP Lumlukka), ', '2019-02-23 03:00:00', 1, 'images/EVP_Media_size_6.png', 0, NULL),
(23, 'Rita Ora Live in Bangkok 2019', NULL, 'THAI WAKE PARK - Lumlukka (TWP Lumlukka), ', '2019-02-23 03:00:00', 1, 'images/PJH_BKK__event_pop_1702ex.jpg', 0, NULL),
(24, 'Rita Ora Live in Bangkok 2019', NULL, 'THAI WAKE PARK - Lumlukka (TWP Lumlukka), ', '2019-02-23 03:00:00', 1, 'images/cover_page2_(1)_(1).jpg', 0, NULL),
(25, 'Gia đình 1', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 5, 'images/giadinh.jpg', 0, '2019-02-25 19:20:12'),
(26, 'Gia đình 2', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 5, 'images/giadinh.jpg', 0, '2019-02-25 19:20:12'),
(27, 'Gia đình 3', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 5, 'images/giadinh.jpg', 0, '2019-02-25 19:20:12'),
(28, 'Gia đình 4', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 5, 'images/giadinh.jpg', 0, '2019-02-25 19:20:12'),
(29, 'Tư thiện 1', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 6, 'images/tuthien.jpg', 0, '2019-02-25 19:20:12'),
(30, 'Tư thiện 2', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 6, 'images/tuthien.jpg', 0, '2019-02-25 19:20:12'),
(31, 'Tư thiện 3', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 6, 'images/tuthien.jpg', 0, '2019-02-25 19:20:12'),
(32, 'Tư thiện 4', NULL, 'Learn Hub Pathum Wan Bangkok Thailand', '2019-02-27 00:00:00', 6, 'images/tuthien.jpg', 0, '2019-02-25 19:20:12');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partners`
--

INSERT INTO `partners` (`id`, `name`, `image`) VALUES
(1, 'Đối tác 1', 'images/doitac.png'),
(2, 'Đối tác 2', 'images/doitac.png'),
(3, 'Đối tác 3', 'images/doitac.png'),
(4, 'Đối tác 4', 'images/doitac.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `questions`
--

INSERT INTO `questions` (`id`, `question`, `content`, `image`) VALUES
(1, 'Eventop là gì ?', 'Git comes with built-in GUI tools (git-gui, gitk), but there are several third-party tools for users looking for a platform-specific experience.', 'images/logo.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `confirmed`, `confirmation_code`, `remember_token`, `created_at`, `updated_at`) VALUES
(9, 'damuoi', 'daocongcan9812@gmail.com', '$2y$10$viXe7LgZwTDKdQyHEVs4mui4W86dD4RpLbw9rA6oaIQcs5D6UVj4y', 0, '155111529015c74241a15e99', NULL, '2019-02-25 10:21:34', '2019-02-25 10:21:34');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT cho bảng `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
