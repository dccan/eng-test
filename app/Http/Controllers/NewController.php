<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\News;
use App\Question;
use App\Partner;
use App\Config;

use Illuminate\Pagination\Paginator;
class NewController extends Controller
{
    //
	
	public function getAll(Request $request) {

		$events = News::where('category_id',1)->orderBy("id",'desc')->paginate(8);

		$slides = News::where('slide',1)->orderBy("id",'desc')->get();

		$business = News::where('category_id',2)->orderBy("id",'desc')->paginate(2);
		$educations = News::where('category_id',3)->orderBy("id",'desc')->paginate(2);
		$news = News::where('category_id',4)->orderBy("id",'desc')->paginate(5);

		$families = News::where('category_id',5)->orderBy("id",'desc')->paginate(2);
		$charities = News::where('category_id',6)->orderBy("id",'desc')->paginate(2);
		
		$question = Question::where('id',1)->first();
		$config = Config::where('id',1)->first();
		$partners = Partner::all();

		$categoryBusiness= Category::where('id',2)->first();
		$categoryEducation= Category::where('id',3)->first();
		$categoryFamilies= Category::where('id',5)->first();
		$categoryCharities= Category::where('id',6)->first();

		// exit();
		$total = $events->lastPage();
		$totalBusiness = $business->lastPage();
		$totalEducations = $educations->lastPage();
		$totalNews = $news->lastPage();
		$totalFamilies = $news->lastPage();
		$totalCharities = $news->lastPage();

		if ($request->ajax()) {
			$category = $request->category;

			if( $category == 'event'){

			   return view('load-more',  compact('events','category') );
			} 
			else if($category == 'business'){
			   return view('load-more',  compact('business','category') );
			}
			else if($category == 'educations'){
			   return view('load-more',  compact('educations','category') );
			}
			else if($category == 'news'){
			   return view('load-more',  compact('news','category') );
			}
			else if($category == 'families'){
			   return view('load-more',  compact('families','category') );
			}
			else if($category == 'charities'){
			   return view('load-more',  compact('charities','category') );
			}
			
		}

		return view('welcome', compact('events','total','totalBusiness','business','educations','totalEducations','news','totalNews','families','charities','totalFamilies','totalCharities','question','partners','config','categoryBusiness','categoryEducation','categoryFamilies','categoryCharities','slides')); 	
	}

}
