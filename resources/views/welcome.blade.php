<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css">
        
    </head>
    
    <body class="fixed-bottom-space landing-theme ng-scope" ng-app="" style="">
    <div class="unobtrusive-flash-container"></div>
    
    <div class="container">
        <nav id="main-nav" class="navbar navbar-inverse navbar-static-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navigation-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <button type="button" class="locales-nav-toggle navbar-toggle" data-toggle="collapse" data-target=".locales-menu">
                EN
                <span class="caret"></span>
                </button>

                <a class="navbar-brand" href="/">
                    <img src="images/logo-top2.png" alt="Blue eventpop logo">
                </a>

                <div class="visible-xs px-3">
                    <div class="clearfix"></div>
                    <form action="/search" method="get" class="mobile event-search">
                        <div class="form-group">
                            <div class="input-group col-sm-12 default-input-group">
                                <input class="form-control" type="search" name="q" placeholder="Event, artist or location">
                                <span class="input-group-btn">
                                    <button class="btn btn-grey btn-outline px-3" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <form action="/search" method="get" class="hidden-xs navbar-left navbar-form event-search">
                <div class="form-group">
                    <div class="input-group col-sm-12 default-input-group">
                        <input class="form-control" type="search" name="q" placeholder="Event, artist or location">
                        <span class="input-group-btn">
                        <button class="btn btn-grey btn-outline text-default px-3" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
            </form>

            <div class="locales-menu navbar-collapse collapse navbar-left">
                <ul class="nav navbar-nav">
                    <li class="visible-xs-block">
                        <a class="locale-selector" data-locale="th" href="https://www.eventpop.me/?locale=th">
                        <img class="flag" src="https://p-a.popcdn.net/assets/locales/th-e933ea7053d0de4727207625712695e5.png" alt="Th">
                        ไทย
                        </a>            
                    </li>
                    <li class="visible-xs-block ">
                        <a class="locale-selector" data-locale="en" href="https://www.eventpop.me/?locale=en">
                        <img class="flag" src="https://p-a.popcdn.net/assets/locales/en-aa5c9fc5825d652a129eeaf26f3c8297.png" alt="En">
                        English
                        <i class="fas fa-check"></i> 
                        </a>            
                    </li>
                </ul>
            </div>

            <div class="navigation-menu navbar-collapse collapse navbar-left">
                <ul class="nav navbar-nav">
                    <li class="visible-xs-block">

                        <a class="open-signin-modal" href="/users/sign_in">Log In / Sign Up</a>
                    </li>
                </ul>
            </div>

            <ul class="nav navbar-nav navbar-right hidden-xs">
                <!-- <li class="locales">
                    <a class="locale locale-selector " data-locale="th" href="https://www.eventpop.me/?locale=th">
                    ไทย
                    </a>            <span class="separator">|</span>
                    <a class="locale locale-selector current" data-locale="en" href="https://www.eventpop.me/?locale=en">
                    English
                    </a>          
                </li> -->
                <li class="locales">
                    <a>
                     <img src="images/flag-us.png" alt="us">
                    </a>
                </li>
                <!-- <li>

                    <a class="btn btn-link navbar-btn open-signin-modal" href="/users/sign_in">Log In / Sign Up</a>
                </li> -->

                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="user" src="images/user.png" alt="login"> 
                @guest
                
                @else   
                 Hi   {{ Auth::user()->name }}
                @endguest

                <span class="caret"></span></a>

                  <ul class="dropdown-menu">
                    @guest
                    <!-- href="{{ route('login') }}" -->
                        <li  data-toggle="modal" data-target="#login" ><a >Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li>
                            <a href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    @endguest
                  </ul>
                </li>
            </ul>


        </nav>

        <header class="hero ignore-navbar-margin-bottom">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                @foreach($slides as $key => $value)
                    @if($key == 0)
                        <li data-target="#myCarousel" data-slide-to="{{$key}}" class="active"></li>
                    @else
                        <li data-target="#myCarousel" data-slide-to="{{$key}}" class=""></li>
                    @endif
                @endforeach
                
                
              </ol>

              <!-- Wrapper for slides -->
              <div class="carousel-inner">

                @foreach($slides as $key => $value)
                    @if($key == 0)
                    <div class="item active">
                      <img src="{{URL::asset($value->image)}}" alt="{{($value->title)}}">
                    </div>
                    @else
                    <div class="item">
                      <img src="{{URL::asset($value->image)}}" alt="{{($value->title)}}">
                    </div>
                    @endif
                @endforeach

              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-2 sk1">
                        <p>Sự kiện</p>
                    </div>
                    <div class="col-md-4 sk2">
                        <p>Tin tức sự kiện</p>
                    </div>
                    <div class="col-md-6 sk3">
                        <p>Sự kiện nổi bật</p>
                    </div>
                </div>
            </div>

        </header>

        
    </div>
    <div class="container">
        <div class="col-md-12 events-block blog" style="background: transparent;">
                <div class="col-md-6 pdLeft">
                    <div class="col-md-12 event-block-item">
                        <div class="col-md-12 text-heading primary-title mb-4 text-primary">
                          <strong>Sự kiện nổi bật</strong>
                        </div>
                        <div class="col-md-12 event">
                            @foreach($events as $event)
                            <div class="home col-xs-12 col-sm-6 col-md-6 event-block-item">
                                <a href="">
                                    <img class="img-responsive" src="{{URL::asset($event->image)}}" alt="{{$event->name}}">
                                    <div class="mt-2">
                                        <h5 class="event-title mb-2">
                                            <strong class="text-default">{{$event->name}}</strong>
                                        </h5>
                                    <small class="event-detail text-muted mt-4">
                                        <div class="small">
                                            <i class="glyphicon glyphicon-time"></i>
                                            {{$event->time_event}}
                                        </div>
                                        <!-- <div class="hidden-md hidden-lg">13 Apr 2019</div> -->
                                        <div class="event-block-address">
                                            <i class="glyphicon glyphicon-map-marker"></i>{{$event->local}}
                                        </div>
                                    </small>
                                    </div>
                                    <div class="spacer-sm"></div>
                                </a>
                            </div>
                            @endforeach    
                        </div>
                        <div class="col-md-12 hr">
                            <div class="row">
                                <hr>      
                            </div>
                            
                        </div>
                        <div class="col-md-12 see-more">
                            <a id="event_more" total="{{$total}}" >Xem thêm</a>
                        </div>
                    </div>

                    <div class="col-md-12 bottom">
                        <div class="row">

                            <img class="img-responsive" src="{{URL::asset($categoryBusiness->image)}}" alt="Title">
                        </div>
                    </div>

                    <div class="col-md-12 bottom ">
                        <div class="row event-block-item">
                            <div class="col-md-12 text-heading primary-title mb-4 text-primary">
                              <strong>Kinh doanh</strong>
                            </div>
                            <div class="col-md-12 business pdLeft">
                                @foreach($business as $busines)
                                <div class="home col-sm-12 col-xs-12">
                                  <div class="row event-block-item past-event mb-4" data-id="3934">
                                        <a href="">
                                          <div class="col-xs-12 col-md-5 ">
                                            <img class="img-responsive cover" src="{{URL::asset($busines->image)}}" alt="{{$busines->name}}">
                                          </div>
                                          <div class="col-xs-12 col-md-7 ">
                                            <div class="mt-2 passed">
                                                <h5 class="event-title mb-2">
                                                    <strong class="text-default">{{$busines->name}}
                                                    </strong>
                                                </h5>
                                                <small class="event-detail text-muted mt-4">
                                                    <div class="small"><i class="glyphicon glyphicon-time"></i>{{$busines->time_event}}
                                                    </div>
                                                    <!-- <div class="hidden-md hidden-lg">01 Sep 2018
                                                    </div> -->
                                                    <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$busines->local}}</div>
                                                </small>
                                            </div>
                                          </div>
                                          
                                        </a>  
                                    </div>
                                </div>
                                @endforeach    
                            </div>
                            <div class="col-md-12 hr">
                                <hr>        
                            </div>
                            <div class="col-md-12 see-more">
                                <a id="business_more" total="{{$totalBusiness}}" >Xem thêm</a>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 bottom">
                        <div class="row">

                            <img class="img-responsive" src="{{URL::asset($categoryEducation->image)}}" alt="Title">
                        </div>
                    </div>

                    <div class="col-md-12 bottom ">
                        <div class="row event-block-item">
                            <div class="col-md-12 text-heading primary-title mb-4 text-primary">
                              <strong>Giáo dục</strong>
                            </div>
                            <div class="col-md-12 educations pdLeft">
                                @foreach($educations as $education)
                                <div class="home col-sm-12 col-xs-12">
                                  <div class="row event-block-item past-event mb-4" data-id="3934">
                                        <a href="">
                                          <div class="col-xs-12 col-md-5 ">
                                            <img class="img-responsive cover" src="{{URL::asset($education->image)}}" alt="{{$education->name}}">
                                          </div>
                                          <div class="col-xs-12 col-md-7 ">
                                            <div class="mt-2 passed">
                                                <h5 class="event-title mb-2">
                                                    <strong class="text-default">{{$education->name}}
                                                    </strong>
                                                </h5>
                                                <small class="event-detail text-muted mt-4">
                                                    <div class=" small"><i class="glyphicon glyphicon-time"></i>{{$education->time_event}}
                                                    </div>
                                                    
                                                    <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$education->local}}</div>
                                                </small>
                                            </div>
                                          </div>
                                          
                                        </a>  
                                    </div>
                                </div>
                                @endforeach    
                            </div>
                            <div class="col-md-12 hr">
                                <hr>        
                            </div>
                            <div class="col-md-12 see-more">
                                <a id="educations_more" total="{{$totalEducations}}" >Xem thêm</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 bottom">
                        <div class="row">

                            <img class="img-responsive" src="{{URL::asset($config->img_app)}}" alt="Title">
                        </div>
                    </div>

                </div>
                
                <div class="col-md-6 pdRight ">
                    <div class="col-md-12">
                    
                    <div class="row event-block-item">
                        <div class="col-md-12 event-block-item">
                        
                            <div class="col-md-12 primary-title text-heading mb-4 text-primary">
                              <strong>Tin tức nổi bật</strong>
                            </div> 
                            <div class="col-md-12 news padding ">
                                @foreach($news as $key => $value)
                                @if($key == 0)
                                    <div class="col-xs-12 col-sm-6 col-md-12 event-block-item">
                                        <a href="/e/4957-s2o">
                                            <img class="img-responsive" src="{{$value->image}}" alt="{{$value->name}}">
                                            <div class="mt-2">
                                                <h5 class="event-title mb-2">
                                                    <strong class="text-default">{{$value->name}}</strong>
                                                </h5>
                                            <small class="event-detail text-muted mt-4">
                                                <div class="small">{{$value->created_at}}</div>
                                            </small>
                                            </div>
                                            <div class="spacer-sm"></div>
                                        </a>
                                    </div>
                                @else
                                    <div class="col-xs-12 col-sm-6 col-md-6 event-block-item">
                                        <a href="/e/4957-s2o">
                                            <img class="img-responsive" src="{{$value->image}}" alt="{{$value->name}}">
                                            <div class="mt-2">
                                                <h5 class="event-title mb-2">
                                                    <strong class="text-default">{{$value->name}}</strong>
                                                </h5>
                                            <small class="event-detail text-muted mt-4">
                                                <div class="small">{{$value->created_at}}</div>
                                            </small>
                                            </div>
                                            <div class="spacer-sm"></div>
                                        </a>
                                    </div>

                                @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="col-md-12 hr">
                            <hr>        
                        </div>
                        <div class="col-md-12 see-more">
                            <a id="news_more" total="{{$totalNews}}" >Xem thêm</a>
                        </div>
                    </div>
                    </div>

                    <div class="col-md-12 bottom">
                        <div class="row">

                            <img class="img-responsive" src="{{URL::asset($categoryFamilies->image)}}" alt="Title">
                        </div>
                    </div>

                    <div class="col-md-12 bottom ">
                        <div class="row event-block-item">
                            <div class="col-md-12 text-heading primary-title mb-4 text-primary">
                              <strong>Gia đình</strong>
                            </div>
                            <div class="col-md-12 families pdLeft">
                                @foreach($families as $value)
                                <div class="home col-sm-12 col-xs-12">
                                  <div class="row event-block-item past-event mb-4" data-id="3934">
                                        <a href="">
                                          <div class="col-xs-12 col-md-5 ">
                                            <img class="img-responsive cover" src="{{URL::asset($value->image)}}" alt="{{$value->name}}">
                                          </div>
                                          <div class="col-xs-12 col-md-7 ">
                                            <div class="mt-2 passed">
                                                <h5 class="event-title mb-2">
                                                    <strong class="text-default">{{$value->name}}
                                                    </strong>
                                                </h5>
                                                <small class="event-detail text-muted mt-4">
                                                    <div class="small"><i class="glyphicon glyphicon-time"></i>{{$value->time_event}}
                                                    </div>
                                                    <!-- <div class="hidden-md hidden-lg">01 Sep 2018
                                                    </div> -->
                                                    <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$value->local}}</div>
                                                </small>
                                            </div>
                                          </div>
                                          
                                        </a>  
                                    </div>
                                </div>
                                @endforeach    
                            </div>
                            <div class="col-md-12 hr">
                                <hr>        
                            </div>
                            <div class="col-md-12 see-more">
                                <a id="families_more" total="{{$totalFamilies}}" >Xem thêm</a>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 bottom">
                        <div class="row">

                            <img class="img-responsive" src="{{URL::asset($categoryCharities->image)}}" alt="Title">
                        </div>
                    </div>

                    <div class="col-md-12 bottom ">
                        <div class="row event-block-item">
                            <div class="col-md-12 text-heading primary-title mb-4 text-primary">
                              <strong>Từ thiện</strong>
                            </div>
                            <div class="col-md-12 charities pdLeft">
                                @foreach($charities as $value)
                                <div class="home col-sm-12 col-xs-12">
                                  <div class="row event-block-item past-event mb-4" data-id="3934">
                                        <a href="">
                                          <div class="col-xs-12 col-md-5 ">
                                            <img class="img-responsive cover" src="{{URL::asset($value->image)}}" alt="{{$value->name}}">
                                          </div>
                                          <div class="col-xs-12 col-md-7 ">
                                            <div class="mt-2 passed">
                                                <h5 class="event-title mb-2">
                                                    <strong class="text-default">{{$value->name}}
                                                    </strong>
                                                </h5>
                                                <small class="event-detail text-muted mt-4">
                                                    <div class=" small"><i class="glyphicon glyphicon-time"></i>{{$value->time_event}}
                                                    </div>
                                                    
                                                    <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$value->local}}</div>
                                                </small>
                                            </div>
                                          </div>
                                          
                                        </a>  
                                    </div>
                                </div>
                                @endforeach    
                            </div>
                            <div class="col-md-12 hr">
                                <hr>        
                            </div>
                            <div class="col-md-12 see-more">
                                <a id="charities_more" total="{{$totalCharities}}" >Xem thêm</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 bottom">
                        <div class="row">

                            <img class="img-responsive" src="{{URL::asset($config->img_call)}}" alt="Title">
                        </div>
                    </div>

                </div>

                <div class="col-md-12 bottom question">
                    <div class="row">
                        
                        <img class="img-responsive" src="{{URL::asset($question->image)}}" alt="Title">
                        
                        <h4>{{$question->question}}</h4>
                        
                        <p>
                            {{$question->content}}
                        </p>

                    </div>
                </div>

                <div class="col-md-12 bottom partners">
                    <div class="row">
                        <h4>Đối tác</h4>
                        @foreach($partners as $value)
                            <img src="{{URL::asset($value->image)}}" alt="{{$value->name}}">
                        @endforeach    
                    </div>
                </div>


                <footer class="col-md-12 bottom footer">
                    <div class="row">
                        {!! $config->footer !!}
                    </div>
                </footer> 
        </div>
    </div>


    <!-- login -->

    

    <!-- Modal -->
    <div id="login" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <!-- <h4 class="modal-title">Login</h4> -->
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="panel panel-default">
                        <div class="panel-heading">Login</div>
                        
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="/authenticate">
                                {{ csrf_field() }}
                                
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Login
                                        </button>

                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Forgot Your Password?
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>


</body>
    
    <script src="{{ asset('/js/jquery-3.1.1.min.js') }}" > </script>
    <script src="{{ asset('/js/home.js') }}" type="text/javascript" charset="utf-8" 
    async defer></script>
    

    

    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <!-- <script src="https://code.jquery.com/jquery-1.11.3.js"></script> -->

    <!-- <script src="https://www.paypalobjects.com/api/checkout.js"></script>
    <script src="https://p-a.popcdn.net/assets/application-fdd3609c0eadf6af4ec4d9c6c946b9c2.js" data-turbolinks-track="true"></script> -->
    
    <script>
        $(document).ready(function() {
          // $('.carousel').slick({
          //   autoplay: true,
          //   dots: true    
          // });

            var pageNumber = 2;
            var pageBusiness = 2;
            var pageEducations = 2;
            var pageNews = 2;
            var pageFamilies = 2;
            var pageCharities = 2;

            $("#families_more").on('click', function(e){
               // e.preventDefault();
               category = 'families';
               loadMoreData(pageFamilies,category);
               pageFamilies +=1;
               var total = $(this).attr('total');

               if(pageFamilies > total ){
                    $(this).hide();
               }
            });

            $("#charities_more").on('click', function(e){
               // e.preventDefault();
               category = 'charities';
               loadMoreData(pageCharities,category);
               pageCharities +=1;
               var total = $(this).attr('total');

               if(pageCharities > total ){
                    $(this).hide();
               }
            });

            $("#event_more").on('click', function(e){
               // e.preventDefault();
               category = 'event';
               loadMoreData(pageNumber,category);
               pageNumber +=1;
               var total = $(this).attr('total');

               if(pageNumber > total ){
                    $(this).hide();
               }
            });

            $("#business_more").on('click', function(e){
               // e.preventDefault();
               category = "business";
               loadMoreData(pageBusiness,category);
               pageBusiness +=1;
               var total = $(this).attr('total');
               if(pageBusiness > total ){
                    $(this).hide();
               }
            });

            $("#educations_more").on('click', function(e){
               // e.preventDefault();
               category = "educations";
               loadMoreData(pageEducations,category);
               pageEducations +=1;
               var total = $(this).attr('total');
               if(pageEducations > total ){
                    $(this).hide();
               }
            });

            $("#news_more").on('click', function(e){
               // e.preventDefault();
               category = "news";
               loadMoreData(pageNews,category);
               pageNews +=1;
               var total = $(this).attr('total');
               if(pageNews > total ){
                    $(this).hide();
               }
            });

            function loadMoreData(page,category){
                $.ajax({
                    type : 'GET',
                    url: "/?page="+page+'&category='+category,
                    dataType : "html",  
                    success : function(data){
                        if(data.length == 0){
                            // :( no more articles
                        }else{
                            $('.'+category).append(data);
                        }
                    },error: function(data){

                    },
                })  
            }
        });
    </script>

    
</html>
