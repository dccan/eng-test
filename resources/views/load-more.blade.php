@if($category == 'event')
    @foreach($events as $event)
        <div class="home col-xs-12 col-sm-6 col-md-6 event-block-item">
            <a href="">
                <img class="img-responsive" src="{{URL::asset($event->image)}}" alt="{{$event->name}}">
                <div class="mt-2">
                    <h5 class="event-title mb-2">
                        <strong class="text-default">{{$event->name}}</strong>
                    </h5>
                <small class="event-detail text-muted mt-4">
                    <div class="hidden-sm hidden-xs small">
                        <i class="glyphicon glyphicon-time"></i>
                        {{$event->time_event}}
                    </div>
                    <!-- <div class="hidden-md hidden-lg">13 Apr 2019</div> -->
                    <div class="event-block-address">
                        <i class="glyphicon glyphicon-map-marker"></i>{{$event->local}}
                    </div>
                </small>
                </div>
                <div class="spacer-sm"></div>
            </a>
        </div>
    @endforeach   

    @elseif($category == 'educations')
        @foreach($educations as $education)
            <div class="home col-sm-12 col-xs-12">
              <div class="row event-block-item past-event mb-4" data-id="3934">
                    <a href="">
                      <div class="col-xs-12 col-md-5 hidden-xs">
                        <img class="img-responsive cover" src="{{URL::asset($education->image)}}" alt="{{$education->name}}">
                      </div>
                      <div class="col-xs-12 col-md-7 hidden-xs">
                        <div class="mt-2 passed">
                            <h5 class="event-title mb-2">
                                <strong class="text-default">{{$education->name}}
                                </strong>
                            </h5>
                            <small class="event-detail text-muted mt-4">
                                <div class="hidden-sm hidden-xs small"><i class="glyphicon glyphicon-time"></i>{{$education->time_event}}
                                </div>
                                <!-- <div class="hidden-md hidden-lg">01 Sep 2018
                                </div> -->
                                <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$education->local}}</div>
                            </small>
                        </div>
                      </div>
                      
                    </a>  
                </div>
            </div>
        @endforeach    
        
    @elseif($category == 'business')    
        @foreach($business as $busines)
            <div class="home col-sm-12 col-xs-12">
              <div class="row event-block-item past-event mb-4" data-id="3934">
                    <a href="">
                      <div class="col-xs-12 col-md-5 hidden-xs">
                        <img class="img-responsive cover" src="{{URL::asset($busines->image)}}" alt="{{$busines->name}}">
                      </div>
                      <div class="col-xs-12 col-md-7 hidden-xs">
                        <div class="mt-2 passed">
                            <h5 class="event-title mb-2">
                                <strong class="text-default">{{$busines->name}}
                                </strong>
                            </h5>
                            <small class="event-detail text-muted mt-4">
                                <div class="hidden-sm hidden-xs small"><i class="glyphicon glyphicon-time"></i>{{$busines->time_event}}
                                </div>
                                <!-- <div class="hidden-md hidden-lg">01 Sep 2018
                                </div> -->
                                <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$busines->local}}</div>
                            </small>
                        </div>
                      </div>
                      
                    </a>  
                </div>
            </div>
        @endforeach    

    @elseif($category == 'news')    
        @foreach($news as $value)
            <div class="col-xs-12 col-sm-6 col-md-6 event-block-item">
                <a href="">
                    <img class="img-responsive" src="{{$value->image}}" alt="{{$value->name}}">
                    <div class="mt-2">
                        <h5 class="event-title mb-2">
                            <strong class="text-default">{{$value->name}}</strong>
                        </h5>
                    <small class="event-detail text-muted mt-4">
                        <div class="small">{{$value->created_at}}</div>
                    </small>
                    </div>
                    <div class="spacer-sm"></div>
                </a>
            </div>
        @endforeach  
    @elseif($category == 'charities')    
            @foreach($charities as $value)
                <div class="home col-sm-12 col-xs-12">
                  <div class="row event-block-item past-event mb-4" data-id="3934">
                        <a href="">
                          <div class="col-xs-12 col-md-5 ">
                            <img class="img-responsive cover" src="{{URL::asset($value->image)}}" alt="{{$value->name}}">
                          </div>
                          <div class="col-xs-12 col-md-7 ">
                            <div class="mt-2 passed">
                                <h5 class="event-title mb-2">
                                    <strong class="text-default">{{$value->name}}
                                    </strong>
                                </h5>
                                <small class="event-detail text-muted mt-4">
                                    <div class=" small"><i class="glyphicon glyphicon-time"></i>{{$value->time_event}}
                                    </div>
                                    
                                    <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$value->local}}</div>
                                </small>
                            </div>
                          </div>
                          
                        </a>  
                    </div>
                </div>
            @endforeach
    @elseif($category == 'families')  
        @foreach($families as $value)
            <div class="home col-sm-12 col-xs-12">
              <div class="row event-block-item past-event mb-4" data-id="3934">
                    <a href="">
                      <div class="col-xs-12 col-md-5 ">
                        <img class="img-responsive cover" src="{{URL::asset($value->image)}}" alt="{{$value->name}}">
                      </div>
                      <div class="col-xs-12 col-md-7 ">
                        <div class="mt-2 passed">
                            <h5 class="event-title mb-2">
                                <strong class="text-default">{{$value->name}}
                                </strong>
                            </h5>
                            <small class="event-detail text-muted mt-4">
                                <div class=" small"><i class="glyphicon glyphicon-time"></i>{{$value->time_event}}
                                </div>
                                
                                <div class="event-block-address"><i class="glyphicon glyphicon-map-marker"></i>{{$value->local}}</div>
                            </small>
                        </div>
                      </div>
                      
                    </a>  
                </div>
            </div>
        @endforeach  

@endif